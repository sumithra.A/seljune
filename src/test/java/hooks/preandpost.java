package hooks;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class preandpost {
	
		public String excelFileName;
		public ChromeDriver driver;

		
		@BeforeMethod
			public  void login () {
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				driver = new ChromeDriver(); 
				driver.get("http://leaftaps.com/opentaps/control/main");
				driver.manage().window().maximize();
		}
				 @AfterMethod
				public  void closebrowser(){
					
					driver.close();
		}
		}
