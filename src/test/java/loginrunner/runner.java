package loginrunner;

	import cucumber.api.CucumberOptions;
	import cucumber.api.testng.AbstractTestNGCucumberTests;
	import cucumber.api.SnippetType;

	@CucumberOptions(		
		features = "src\\test\\java\\loginfeatures",
		glue= {"loginsteps","hooks"},
/*		dryRun = false,
		snippets = SnippetType.CAMELCASE,
*/		monochrome = true)
	public class runner extends AbstractTestNGCucumberTests {

	}


