package week3.day2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import createlead.ReadExcel;

public class preandpost {
	public String excelFileName;
	public ChromeDriver driver;

	@Parameters({"username","password"})
	@BeforeMethod
		public  void log(String username,String password) {
			System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
			driver = new ChromeDriver(); 
			driver.get("http://leaftaps.com/opentaps/control/main");
			driver.manage().window().maximize();
			WebElement eleUserName= driver.findElementById("username");
			eleUserName.sendKeys(username);
			driver.findElementById("password").sendKeys(password);
			driver.findElementByClassName("decorativeSubmit").click();
			//driver.findElementByClassName("decorativeSubmit").click();
			
}
			@AfterMethod
			public  void closeApp(){
				
				driver.close();
	}
			
			
			@DataProvider(name="dataSupplier")
			public String[][] getData() throws IOException
			{
				String[][]data= ReadExcel.getExcelData(excelFileName);
			return data;
					
				}
}