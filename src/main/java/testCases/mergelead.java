package testCases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class mergelead {

	

	public static  void main (String[]args) throws InterruptedException 
	{
		
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(); 
		driver.manage().window().maximize();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		WebElement eleUserName= driver.findElementById("username");
		eleUserName.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		//driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		
	
        driver.findElementByLinkText("Merge Leads").click();
        
        driver.findElementByXPath("(//img[@alt=\'Lookup\'])[1]").click();
        Set<String> win1=driver.getWindowHandles();
        List<String> win =new ArrayList<String>(win1);
        String L = win.get(1);
        driver.switchTo().window(L);
       
        Thread.sleep(1000);
        System.out.println(driver.getTitle());
        driver.findElementByXPath("//input[@name=\"firstName\"]").sendKeys("sarma");  
        driver.findElementByXPath("//button[@class=\"x-btn-text\"]").click();
        Thread.sleep(1000);
        String ID = ((WebElement) driver.findElementsByXPath("//table[@class=\"x-grid3-row-table\"]//tr[1]//td[1]")).getText();
        System.out.println(ID);
        driver.findElementByXPath("//table[@class=\"twoColumnform\"]//tr[2]//img").click();
        
        L= win.get(0);
        driver.switchTo().window((String) L);
        System.out.println(driver.getTitle());
        
        driver.findElementByClassName("buttondangerous").click();
        driver.switchTo().alert().accept();
        
        driver.findElementByLinkText("Find Leads").click();
         ((WebElement) driver.findElementsByXPath("//button[text()='Find Leads']")).click();
         
         Thread.sleep(2000);
        String e=driver.findElementByXPath("//div[@class=\"x-toolbar x-small-editor\"]//div").getText();
        System.out.println("Error shown:"+e);
        Thread.sleep(3000);
        driver.quit();
        
        
	}

}
