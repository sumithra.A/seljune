package testCases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import week3.day2.preandpost;

public class DuplicateLead extends preandpost
{
	public static  void main (String[]args)throws InterruptedException 
	
	{
	System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver(); 
	driver.manage().window().maximize();
		
	driver.get("http://leaftaps.com/opentaps/control/main");
		
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	//driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByLinkText("Leads").click();
	driver.findElementByLinkText("Find Leads").click();
	driver.findElementByLinkText("Email").click();
	driver.findElementByName("emailAddress").sendKeys("xyz@gmail.com");
	driver.findElementByXPath("//button[text()=\"Find Leads\"]").click();
	driver.findElementByLinkText("MUNAGULA").getText();
	driver.findElementByLinkText("MUNAGULA").click();
	driver.findElementByLinkText("Duplicate Lead").click();
	driver.findElementByName("submitButton").click();
	}

}

