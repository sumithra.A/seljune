package createlead;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static String[][] getExcelData(String excel) throws IOException {
		
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+excel+".xlsx");
		XSSFSheet sheet =wbook.getSheetAt(0);
		int rowcount=sheet.getLastRowNum();
		int columnCount = sheet.getRow(0).getLastCellNum();
		String[][]data = new String[rowcount][columnCount];
		System.out.println(rowcount+":"+columnCount);
		for(int i=1;i<=rowcount;i++)
		{
	     XSSFRow row = sheet.getRow(i);
			
			for(int j =0;j<columnCount;j++)
			{
				XSSFCell cell =row.getCell(j);
				String value= cell.getStringCellValue();
				data[i-1][j]=value;
				
			}
		}
		return data;
	}
}